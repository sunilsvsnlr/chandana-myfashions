﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace MyFashions1Nov.Converters
{
    public class VisibilityforJewellery:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long prodTemplateID = (long)value;
            if (prodTemplateID > 0)
            {
                CreateDBEntity.Models.productTemplateBeans prodTemplateBean = StoreCompare_Cart.lstCart.Where(c => c.ProdTemplateID == prodTemplateID).SingleOrDefault();
                if (prodTemplateBean.category.Name.Contains("JEWELLERY"))
                {
                    return Visibility.Visible;
                }
                else
                    return Visibility.Collapsed;
            }
            else
                return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
