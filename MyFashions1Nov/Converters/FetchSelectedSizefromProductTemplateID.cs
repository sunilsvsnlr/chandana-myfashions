﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MyFashions1Nov.Converters
{
    public class FetchSelectedSizefromProductTemplateID:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long key = System.Convert.ToInt64(value);
            if (StoreCompare_Cart.lstMaintainSelectedSizes.ContainsKey(key))
            {
                return "Seller Size: " + System.Convert.ToString(StoreCompare_Cart.lstMaintainSelectedSizes[key]);
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
