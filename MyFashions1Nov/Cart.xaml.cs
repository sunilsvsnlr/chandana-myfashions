﻿using CreateDBEntity.DBFolder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for Cart.xaml
    /// </summary>
    public partial class Cart : UserControl
    {
        Decimal GrandTotal;
        public Cart()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ImageContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();

            CheckCartIsEmpty();
            decimal totAmt = 0;
            foreach (CreateDBEntity.Models.productTemplateBeans item in StoreCompare_Cart.lstCart.ToList())
            {
                if (!item.category.Name.Contains("JEWELLERY") && !item.subCategory.Name.Contains("SAREES"))//sarees total is not added due to request made on 10-2-2015
                {
                    if (item.discount > 0)
                        totAmt += System.Convert.ToDecimal((item.TemplatePrice - (item.TemplatePrice * (item.discount / 100))));
                    else
                        totAmt += item.TemplatePrice;
                }
            }

            txtTotalamt.Text = Convert.ToString(Math.Round(totAmt));
            ScrollViewer.ScrollToVerticalOffset(0);
            
        }

        private void CheckCartIsEmpty()
        {
            if (ImageContext.Items.Count == 0)
            {
                imgCartEmpty.Visibility = Visibility.Visible;
                grdGrandTotal.Visibility = Visibility.Collapsed;
            }
            else
            {
                imgCartEmpty.Visibility = Visibility.Hidden;
                grdGrandTotal.Visibility = Visibility.Visible;
            }
        }
        
        public event EventHandler MyEventCartCntUpdate;
        StackPanel stckpnlClickedRemoveBtn;

        private void StackPanel_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            stckpnlClickedRemoveBtn = sender as StackPanel;
        }

        public event EventHandler MyEventCmpreCntUpdate;

        private void BtnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
           
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void BtnRemove_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts when Remove Button Enters in CartPage");
                Button btnSelectedRemove = sender as Button;
                CreateDBEntity.Models.productTemplateBeans samp = btnSelectedRemove.DataContext as CreateDBEntity.Models.productTemplateBeans;

                StoreCompare_Cart.lstCart.Remove(samp);

                StoreCompare_Cart.cartCnt = StoreCompare_Cart.lstCart.Count;

                ImageContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                CheckCartIsEmpty();
                MyEventCartCntUpdate(this, null);

                CalculateTotal();

                LogHelper.Logger.Info("Stops when Remove Button Enters in CartPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CartPage : BtnRemove_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }

        }

        public void CalculateTotal()
        {
            if (StoreCompare_Cart.lstCart.Count > 0)
            {

                foreach (var item in StoreCompare_Cart.lstCart)
                {
                    if (!item.category.Name.Contains("JEWELLERY") && !item.subCategory.Name.Contains("SAREES")) //sarees total is not added due to request made on 10-2-2015
                    {
                        if (item.discount > 0)
                            GrandTotal += Math.Round(System.Convert.ToDecimal((item.TemplatePrice - (item.TemplatePrice * (item.discount / 100)))));
                        else
                            GrandTotal += Math.Round(item.TemplatePrice);
                    }
                }
                txtTotalamt.Text = GrandTotal.ToString();
                GrandTotal = 0;
            }
            else
            {
                txtTotalamt.Text = "0";
            }
        }

        private void BtnCompare_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts when Add To Compare Button Enters in CartPage");
                bool isSuccess = false;

                Button btnSelectedProduct = sender as Button;

                CreateDBEntity.Models.productTemplateBeans samp = btnSelectedProduct.DataContext as CreateDBEntity.Models.productTemplateBeans;

                List<CreateDBEntity.Models.productTemplateBeans> lstprod = StoreCompare_Cart.lstCompare.Where(c => c.ProdTemplateID.Equals(samp.ProdTemplateID)).ToList();
                List<CreateDBEntity.Models.productTemplateBeans> lstSelCmp = StoreCompare_Cart.lstSelectedCompare.Where(c => c.ProdTemplateID.Equals(samp.ProdTemplateID) && c.SNo.Equals(samp.SNo)).ToList();
                if (lstprod.Count == 0 && lstSelCmp.Count == 0)
                {
                    isSuccess = true;
                    StoreCompare_Cart.lstCompare.Add(samp);
                    StoreCompare_Cart.CmpreCnt += 1;
                    StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                    StoreCompare_Cart.lstCart.Remove(samp);
                    StoreCompare_Cart.cartCnt = StoreCompare_Cart.lstCart.Count;
                    ImageContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                    CheckCartIsEmpty();
                    MyEventCartCntUpdate(this, null);
                    MyEventCmpreCntUpdate(this, null);
                    Button btn = sender as Button;
                    btn.Visibility = Visibility.Hidden;
                }
                else
                {
                    MessageBox.Show("Already added the selected item to compare. Please choose another one....", "My Fashions", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                CalculateTotal();

                if (isSuccess)
                {
                    MyEventCmpreCntUpdate(this, null);
                }
                LogHelper.Logger.Info("Stops when AddToCompare Button Enters in CartPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CartPage : BtnCompare_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }

        }

        private void sellerSizes_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

    }
}
